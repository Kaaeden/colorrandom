package com.example.colorrandomizer

import androidx.lifecycle.ViewModel
import kotlin.random.Random
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainViewModel : ViewModel() {

    private val _color = MutableStateFlow(mutableListOf(255, 255, 255))
    val colors: StateFlow<MutableList<Int>> get() = _color

    fun randomColor()
    {
        var r_ = Random.nextInt(0,255)
        var g_ = Random.nextInt(0,255)
        var b_ = Random.nextInt(0, 255)

        _color.value = mutableListOf(r_, g_, b_)
    }

    fun resetColor()
    {
        _color.value = mutableListOf(240,255,255)
    }

}