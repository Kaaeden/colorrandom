package com.example.colorrandomizer

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModel
import com.example.colorrandomizer.ui.theme.ColorRandomizerTheme

class MainActivity : ComponentActivity() {

    private val mainViewModel by viewModels<MainViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ColorRandomizerTheme {
                // A surface container using the 'background' color from the theme
                val colors: MutableList<Int> by mainViewModel.colors.collectAsState()
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    TapScreen(colors)
                }
            }
        }
    }

    @Composable
    @Preview(showBackground = true)
    fun TapScreen(colors_ : MutableList<Int> = mutableListOf(240, 255,255))
    {


        Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
            Box(modifier = Modifier
                .pointerInput(this) {

                    detectTapGestures(
                        onTap = { mainViewModel.randomColor() },
                        onLongPress = { mainViewModel.resetColor() })
                }
                .background(
                    color = Color(
                        red = colors_[0],
                        green = colors_[1],
                        blue = colors_[2]
                    )
                )
                .fillMaxSize()
            ) {

                Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center)
                {

                    CircleButton()
                }

            }




        }
    }

    @Composable
    fun CircleButton()
    {
        Box(
            modifier = Modifier
                .size(64.dp)
                .clip(shape = CircleShape)
                .background(color = Color(red = 240, green = 10, blue = 10))
                .fillMaxSize()
        ) { }


    }

}




